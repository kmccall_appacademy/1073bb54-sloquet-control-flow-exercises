# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lowercase_ltrs = ("a".."z").to_a
  str.chars.reject {|char| lowercase_ltrs.include?(char)}.join('')
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  return str[str.length/2] if str.length.odd?
  str[(str.length/2)-1..(str.length/2)]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel_count = str.chars.inject(0) do |count, ltr|
    count += 1 if VOWELS.include?(ltr)
    count
  end
  vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  while num > 0
    product *= num
    num -= 1
  end
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str_joined = ''
  arr.each_with_index do |x, idx|
    if idx == arr.count-1
      str_joined += x
    else
      str_joined += (x + separator)
    end
  end
  str_joined
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_str = str.chars.map.with_index do |char, idx|
    if idx.even?
      char.downcase
    else
      char.upcase
    end
  end
  new_str.join('')
end

# Reverse all words of five or more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(' ')
  words.map! do |word|
    if word.length > 4
      word.reverse
    else
      word
    end
  end
  words.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).map do |num|
    if num % 3 == 0 && num % 5 == 0
      "fizzbuzz"
    elsif num % 3 == 0
      "fizz"
    elsif num % 5 == 0
      "buzz"
    else
      num
    end
  end
end

# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  #arr.reverse
  new_arr = []
  arr.each {|x| new_arr.unshift(x)}
  new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  possible_factor = num/2
  while possible_factor > 1
    return false if num % possible_factor == 0
    possible_factor -= 1
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors_arr = []
  x = 1
  while x <= num
    factors_arr << x if num % x == 0
    x += 1
  end
  factors_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select {|factor| prime?(factor)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  counter_even = 0
  counter_odd = 0
  first_odd = nil
  first_even = nil
  x = 0
  while x < arr.length
    if arr[x].even?
      counter_even+=1
      unless first_even
        first_even = arr[x]
      end
    else
      counter_odd += 1
      unless first_odd
        first_odd = arr[x]
      end
    end
    return first_even if counter_odd > 1 && first_even
    return first_odd if counter_even > 1 && first_odd
    x += 1
  end
end
